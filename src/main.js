import Vue from 'vue'
import App from './App.vue'
import ToggleButton from 'vue-js-toggle-button'
import Toaster from 'v-toaster'
import Vuetify from 'vuetify'
import VueFuse from 'vue-fuse'

import 'v-toaster/dist/v-toaster.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faLaptopCode,
  faSave,
  faTrash,
  faEye,
  faEyeSlash,
  faCheckCircle,
  faCircle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
  faLaptopCode,
  faSave,
  faTrash,
  faEye,
  faEyeSlash,
  faCheckCircle,
  faCircle
)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Toaster, {timeout: 3000})

Vue.config.productionTip = false
Vue.use(ToggleButton)
Vue.use(Vuetify)
Vue.use(VueFuse)

new Vue({
  render: h => h(App)
}).$mount('#app')
